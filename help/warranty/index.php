<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Гарантия на товар");
?><p>
</p>
 1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Качество поставляемого товара должно соответствовать стандартам и техническим условиям заводов-изготовителей данного товара.
<p>
	 2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Товар, при его заказе его с нормами упаковки, должен быть укакован в соответствии с требованиями заводов-изготовителей для обеспечения его сохранности при перевозке.<br>
</p>
<p>
	 &nbsp;3.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Проверка товара перед отправкой покупателю:
</p>
<p>
	 Наши уполномоченные сотрудники, при комплектации заказа, проверяют целостность, точное количество и упаковку товара.
</p>
<p>
	 4.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; При выявлении расхождений количественных показателей и дефектов товаров, Вы незамедлительно сообщаете об этом нам. Сохраняйте, пожалуйста, чек об оплате – это необходимо чтобы мы быстро смогли Вам помочь.
</p>
<p>
	 5.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Рекламации по недостаткам поставленного товара, на которые производителями установлен гарантийный срок, выявленные в процессе использования товара по прямому назначению, направляются непосредственно производителю товара в пределах установленного гарантийного срока.
</p>
<p>
	 6.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Товар ненадлежащего качества, с не истекшим гарантийным сроком, установленным заводом-изготовителем, подлежит замене с нашей стороны в разумный срок, после подтверждения данного факта нашим уполномоченным представителем.
</p>
<p>
	 7.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Срок обмена и возврата товара устанавливается в соответствии с законом Защиты Прав Потребителя в РФ. Обращаем Внимание на <a href="http://www.consultant.ru/document/cons_doc_LAW_373622/a7e95958f6d9c791e728172bd17d5df3b1152118/#dst100157">товар</a>, который по закону не подлежит обмену и возврату.
</p>
<p>
	 &nbsp;
</p>
 Если Вы обнаружили недостачу или брак товара – не беспокойтесь, мы в максимально короткие сроки разрешим ситуацию. Обратитесь, пожалуйста по адресу электронной почты, или телефону <a href="https://ural-krepej.ru/contacts/">магазина</a>, в котором Вы приобретали товар.&nbsp; <br>
 <br>
 Если товар приобретался онлайн в интернет-магазине – сообщите о проблеме на почту <a href="mailto:ural_krepej@mail.ru">ural_krepej@mail.ru</a><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>