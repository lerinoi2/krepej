<?php

CModule::AddAutoloadClasses(
    'aspro.max',
    array(
        'CMax' => 'classes/general/CMax.php')
);

use \Bitrix\Main\Application,
    \Bitrix\Main\Type\Collection,
    \Bitrix\Main\Loader,
    \Bitrix\Main\IO\File,
    \Bitrix\Main\Localization\Loc,
    \Bitrix\Main\Config\Option;


class CustomContact extends CMax {
    public static function ShowMobileMenuContacts(){
        global $APPLICATION, $arRegion, $arTheme;
        $arBackParametrs = self::GetBackParametrsValues(SITE_ID);
        $iCountPhones = ($arRegion ? count($arRegion['PHONES']) : $arBackParametrs['HEADER_PHONES']);
        $regionID = ($arRegion ? $arRegion['ID'] : '');
        ?>
        <?if($iCountPhones): // count of phones?>
            <?
            $phone = ($arRegion ? $arRegion['PHONES'][0] : $arBackParametrs['HEADER_PHONES_array_PHONE_VALUE_0']);
            $href = 'tel:'.str_replace(array(' ', '-', '(', ')'), '', $phone);

            $description = ($arRegion ? $arRegion['PROPERTY_PHONES_DESCRIPTION'][0] : $arBackParametrs['HEADER_PHONES_array_PHONE_DESCRIPTION_0']);
            $description = (!empty($description)) ? '<span class="descr">' . $description . '</span>' : '';

            static $mphones_call;

            $iCalledID = ++$mphones_call;
            ?>

            <?if($arRegion):?>
                <?$frame = new \Bitrix\Main\Page\FrameHelper('mobile-phone-block'.$iCalledID);?>
                <?$frame->begin();?>
            <?endif;?>

            <!-- noindex -->
            <div class="menu middle mobile-menu-contacts">
                <ul>
                    <li>
                        <a rel="nofollow" href="<?=($iCountPhones > 1 ? '' : $href)?>" class="dark-color<?=($iCountPhones > 1 ? ' parent' : '')?> <?=(empty($description)?'no-decript':'decript')?>">
                            <i class="svg svg-phone"></i>
                            <?=CMax::showIconSvg("phone", SITE_TEMPLATE_PATH.'/images/svg/Phone_black.svg', '', '', true, false);?>
                            <span><?=$phone?><?=$description?></span>
                            <?if($iCountPhones > 1):?>
                                <span class="arrow">
									<?=CMax::showIconSvg("triangle", SITE_TEMPLATE_PATH.'/images/svg/trianglearrow_right.svg', '', '', true, false);?>
								</span>
                            <?endif;?>
                        </a>
                        <?if($iCountPhones > 1): // if more than one?>
                            <ul class="dropdown">
                                <li class="menu_back"><a href="" class="dark-color" rel="nofollow"><?=CMax::showIconSvg('back_arrow', SITE_TEMPLATE_PATH.'/images/svg/return_mm.svg')?><?=Loc::getMessage('MAX_T_MENU_BACK')?></a></li>
                                <li class="menu_title"><?=Loc::getMessage('MAX_T_MENU_CALLBACK')?></li>
                                <?for($i = 0; $i < $iCountPhones; ++$i):?>
                                    <?
                                    $phone = ($arRegion ? $arRegion['PHONES'][$i] : $arBackParametrs['HEADER_PHONES_array_PHONE_VALUE_'.$i]);
                                    $href = 'tel:'.str_replace(array(' ', '-', '(', ')'), '', $phone);

                                    $description = ($arRegion ? $arRegion['PROPERTY_PHONES_DESCRIPTION'][$i] : $arBackParametrs['HEADER_PHONES_array_PHONE_DESCRIPTION_'.$i]);
                                    $description = (!empty($description)) ? '<span class="descr">' . $description . '</span>' : '';
                                    ?>
                                    <li><a rel="nofollow" href="<?=$href?>" class="bold dark-color <?=(empty($description)?'no-decript':'decript')?>"><?=$phone?><?=$description?></a></li>
                                <?endfor;?>
                                <?if($arTheme['SHOW_CALLBACK']['VALUE'] == 'Y'):?>
                                    <li><a rel="nofollow" class="dark-color" href="" data-event="jqm" data-param-form_id="CALLBACK" data-name="callback"><?=Loc::getMessage('CALLBACK')?></a></li>
                                <?endif;?>
                            </ul>
                        <?endif;?>
                    </li>
                </ul>
            </div>
            <!-- /noindex -->

            <?if($arRegion):?>
                <?$frame->end();?>
            <?endif;?>

        <?endif;?>
        <div class="contacts">
            <div class="title"><?=Loc::getMessage('MAX_T_MENU_CONTACTS_TITLE')?></div>

            <?if($arRegion):?>
                <?$frame = new \Bitrix\Main\Page\FrameHelper('mobile-contact-block');?>
                <?$frame->begin();?>
            <?endif;?>

            <?if($arRegion):?>
                <?if($arRegion['PROPERTY_ADDRESS_VALUE']):?>
                    <div class="address">
                        <?=self::showIconSvg("address", SITE_TEMPLATE_PATH."/images/svg/address.svg");?>
                        <?=$arRegion['PROPERTY_ADDRESS_VALUE']['TEXT'];?>
                    </div>
                <?endif;?>
            <?else:?>
                <div class="address">
                    <?=self::showIconSvg("address", SITE_TEMPLATE_PATH."/images/svg/address.svg");?>
                    <?$APPLICATION->IncludeFile(SITE_DIR."include/top_page/site-address.php", array(), array(
                            "MODE" => "html",
                            "NAME" => "Address",
                            "TEMPLATE" => "include_area.php",
                        )
                    );?>
                </div>
            <?endif;?>
            <?if($arRegion):?>
                <?if($arRegion['PROPERTY_EMAIL_VALUE']):?>
                    <div class="email">
                        <?=self::showIconSvg("email", SITE_TEMPLATE_PATH."/images/svg/email_footer.svg");?>
                        <?foreach($arRegion['PROPERTY_EMAIL_VALUE'] as $value):?>
                            <a href="mailto:<?=$value;?>"><?=$value;?></a>
                        <?endforeach;?>
                    </div>
                <?endif;?>
            <?else:?>
                <div class="email">
                    <?=self::showIconSvg("email", SITE_TEMPLATE_PATH."/images/svg/email_footer.svg");?>
                    <?$APPLICATION->IncludeFile(SITE_DIR."include/footer/site-email.php", array(), array(
                            "MODE" => "html",
                            "NAME" => "Address",
                            "TEMPLATE" => "include_area.php",
                        )
                    );?>
                </div>
            <?endif;?>

            <?if($arRegion):?>
                <?$frame->end();?>
            <?endif;?>

        </div>
    <?}
    
}

?>